public class Carro {

    public static final String VERMELHO = "vermelho";
    public static final String PRETA = "preta";
    public static final String ALCOOL = "alcool";
    public static final String GASOLINA = "gasolina";

    private Integer quantidadePneus;
    private Integer quantidadeCalotas;
    private Integer quantidadeParafusosPneus;
    private Integer quantidadePortas;
    private Integer numeroChassi;
    private Integer anoFabricacao;
    private Integer combustivel;

    public Carro(Integer quantidadePneus) {
        setQuantidadePneus(quantidadePneus);
    }

    public Integer getQuantidadePneus() {
        return quantidadePneus + 2;
    }
    public void setQuantidadePneus(Integer quantidadePneus) {
        quantidadeCalotas = quantidadePneus;
        quantidadeParafusosPneus = quantidadePneus * 4;
        this.quantidadePneus = quantidadePneus;

    }

    public Integer getQuantidadePortas(int i) {
        return quantidadePortas = 4;
    }

    public void setQuantidadePortas(Integer quantidadePortas) {
        this.quantidadePortas = quantidadePortas;
    }

    public Integer getNumeroChassi(int i) {
        return numeroChassi = 123456;
    }

    public void setNumeroChassi(Integer numeroChassi) {
        this.numeroChassi = numeroChassi;
    }

    public Integer getAnoFabricacao() {
        return anoFabricacao = 160696;
    }

    public void setAnoFabricacao(Integer anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }

    public void setCor(String cor) {
        System.out.println(cor);
    }

    public void SetCombustivel(String combustivel) {
        System.out.println(combustivel);
    }

    public Integer getQuantidadeCalotas() {
        return quantidadeCalotas;
    }

    public void setQuantidadeCalotas(Integer quantidadeCalotas) {
        this.quantidadeCalotas = quantidadeCalotas;
    }

    public Integer getQuantidadeParafusosPneus() {
        return quantidadeParafusosPneus;
    }

    public void setQuantidadeParafusosPneus(Integer quantidadeParafusosPneus) {
        this.quantidadeParafusosPneus = quantidadeParafusosPneus;
    }


    public void imprimeValores() {
        System.out.println("Quantidade Pneus " + getQuantidadePneus());
        System.out.println("Quantidade de Calotas" + getQuantidadeCalotas());
        System.out.println("Quantidade de parafuso pneus" + getQuantidadeParafusosPneus());
        System.out.println("Quantidade Portas " + getQuantidadePortas(4));
        System.out.println("Número do Chassi " + getNumeroChassi(123456));
        System.out.println("Ano de fabricação " + getAnoFabricacao());
    }
}
